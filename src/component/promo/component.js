import React from 'react';
import Banner from '../../asset/banner.png';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

function Benner(props) {
  const { classes } = props;
  return (
    <React.Fragment>
      <CardMedia image={Banner} className={classes.media}>
        <div className={classes.label}>
          <Typography className={classes.promo}>Promo</Typography>
        </div>
      </CardMedia>
    </React.Fragment>
  );
}

export default Benner;
