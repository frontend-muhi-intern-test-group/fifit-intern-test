const styles = {
  media: {
    maxWidth: 448,
    height: '178px',
    marginTop: '95px',
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    borderRadius: '5px'
  },
  label: {
    marginLeft: 10,
    paddingTop: '32%',
    display: 'flex'
  },
  promo: {
    color: '#FFFFFF',
    backgroundColor: '#109666',
    borderRadius: '5px',
    height: '26px',
    width: '74px',
    textAlign: 'center'
  }
};
export default styles;
