import React, { useState } from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Ceklis from '../../asset/cekliss.png';

function Snack(props) {
  const { classes } = props;
  const [number, setNumber] = useState(0);
  const { item } = props;
  const handlePlus = () => {
    setNumber(number + 1);
  };
  const handleMinus = () => {
    setNumber(number - 1);
  };
  const handleTambah = () => {
    setNumber(number + 1);
  };

  const hidenButton = () => {
    if (number == 0) {
      return {
        backgroundColor: '#5E52AD',
        width: '10px',
        height: '30px',
        backgroundColor: '#5E52AD',
        marginBottom: '-50px',
        marginLeft: '30px'
      };
    }
    if (number !== 0) {
      return {
        display: 'none'
      };
    }
  };

  const hideCount = () => {
    if (number == 0) {
      return {
        display: 'none'
      };
    }
    if (number !== 0) {
      return {
        display: 'flex'
      };
    }
  };

  return (
    <React.Fragment className={classes.container}>
      <Typography variant="h5" className={classes.judul}>
        {item.judul}
      </Typography>
      <Grid
        container
        spacing={0}
        style={{ paddingTop: '20px', paddingBottom: '20px' }}>
        <Grid item xs={1}>
          <Paper style={{ width: '100px', height: '100px' }}>
            <img src={item.image} className={classes.list} />
          </Paper>
        </Grid>
        <Grid item xs={11} style={{ paddingLeft: '80px' }}>
          <Typography className={classes.nama}>{item.nama}</Typography>
          <Typography className={classes.karakter}>{item.karakter}</Typography>
          <Typography className={classes.harga}>
            {item.harga}
            <Grid className={classes.GridBtn}>
              <Button onClick={() => handleTambah()} style={hidenButton()}>
                <Typography className={classes.Tambah}>Tambah</Typography>
              </Button>
              <Grid style={hideCount()} container spacing={0}>
                <Button
                  onClick={() => handleMinus()}
                  className={classes.buttonMinus}>
                  <Typography className={classes.minus}> - </Typography>
                </Button>
                <h3 className={classes.angka}>{number}</h3>
                <Button
                  onClick={() => handlePlus()}
                  className={classes.buttonPlus}>
                  <Typography className={classes.plus}> + </Typography>
                </Button>
                <img
                  src={Ceklis}
                  style={{ marginTop: -50, height: 28, marginLeft: -120 }}
                />
              </Grid>
            </Grid>
          </Typography>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default Snack;
