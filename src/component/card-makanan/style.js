const styles = {
  judul: {
    variant: 'h5',
    paddingTop: '20px',
    fontWeight: 'bold',
    fontSize: '16px'
  },
  list: {
    width: '100px',
    height: '100px',
    paddingBottom: '20px'
  },
  nama: {
    paddingTop: '2px',
    fontWeight: 'bold',
    fontSize: '16px'
  },
  karakter: {
    paddingTop: '2px',
    fontSize: '12px',
    color: '#878282'
  },
  harga: {
    paddingTop: '20px',
    fontSize: '16px',
    color: '#000000'
  },
  GridBtn: {
    marginLeft: '105px',
    marginTop: '-70px',
    position: 'absolute'
  },
  Tambah: {
    fontSize: '15px',
    color: 'white',
    textTransform: 'none'
  },
  Number: {
    marginLeft: '50px',
    marginTop: 30,
    position: 'absolute',
    color: '#5E52AD'
  },
  container: {
    borderStyle: 'groove',
    borderWidth: '1px'
  },
  buttonMinus: {
    marginLeft: '10px',
    marginTop: '28px',
    boxShadow: '0px 1px 4px rgba(109, 96, 96, 0.25)',
    width: '5px',
    height: '20px'
  },
  minus: {
    marginLeft: -45,
    marginTop: -7,
    color: '#5E52AD'
  },
  buttonPlus: {
    marginLeft: '30px',
    marginTop: '-46px',
    boxShadow: '0px 1px 4px rgba(109, 96, 96, 0.25)',
    width: '5px',
    height: '20px'
  },
  angka: {
    marginLeft: '-29px',
    marginTop: '25px',
    color: '#5E52AD'
  },
  plus: {
    marginLeft: 40,
    marginTop: -7,
    color: '#5E52AD'
  }
};
export default styles;
