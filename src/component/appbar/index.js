import { withStyles } from '@material-ui/core/styles';
import Component from './component';
import styles from './style';

const Styled = withStyles(styles)(Component);
export default Styled;
