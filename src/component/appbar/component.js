import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Back from '../../asset/back.png';
import Toolbar from '@material-ui/core/Toolbar';
import { withRouter } from 'react-router-dom';
import LocationOnIcon from '@material-ui/icons/LocationOn';

function Appbar(props) {
  const { classes } = props;
  const handleBack = () => {
    props.history.push('/');
  };

  return (
    <Box display="flex" justifyContent="center">
      <AppBar className={classes.appbar} position="static">
        <Toolbar>
          <img src={Back} className={classes.kembali} onClick={handleBack} />
          <Typography className={classes.antar}>Antar ke</Typography>
          <Typography className={classes.location}>
            <LocationOnIcon className={classes.icon} />
            Hotel Dafam Semarang
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
export default withRouter(Appbar);
