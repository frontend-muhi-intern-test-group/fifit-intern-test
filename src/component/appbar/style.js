const styles = {
  appbar: {
    position: 'static',
    maxWidth: 448,
    position: 'fixed',
    backgroundColor: 'white',
    height: '79px'
  },
  antar: {
    variant: 'body2',
    paddingLeft: '20px',
    marginTop: '-15px',
    color: 'black',
    fontSize: '12px'
  },
  location: {
    paddingTop: '26px',
    marginLeft: '-50px',
    color: 'black',
    fontWeight: 'bold',
    fontSize: '18px'
  },
  icon: {
    color: '#F56363',
    width: '16px',
    height: '16px',
    marginRight: '8px',
    paddingTop: '15px'
  },
  kembali: {
    width: '20px',
    height: '20px',
    paddingTop: '15px'
  }
};
export default styles;
