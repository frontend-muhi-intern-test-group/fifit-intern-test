import { withStyles } from '@material-ui/core/styles';
import Component from './component';
import styles from './style';

const Style = withStyles(styles)(Component);
export default Style;
