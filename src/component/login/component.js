import React from 'react';
import Makan from '../../asset/Group 10.png';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';

function Login(props) {
  const { classes } = props;
  return (
    <Container
      className={classes.container}
      style={{
        padding: 0,
        backgroundColor: 'white'
      }}
      maxWidth="xs">
      <Grid container spacing={0}>
        <Grid
          item
          xs={12}
          style={{ display: 'flex', justifyContent: 'center' }}>
          <img src={Makan} className={classes.diner} />
        </Grid>
      </Grid>
      <Grid item xs={12} className={classes.gridAtas}>
        <Typography className={classes.wellcome}>Selamat Datang</Typography>
        <Typography className={classes.text}>
          Masuk untuk menikmati makanan yang tersedia
        </Typography>
      </Grid>
      <Grid item xs={12} style={{ display: 'flex', justifyContent: 'center' }}>
        <Button
          onClick={() => {
            props.history.push('/list-makanan');
          }}
          variant="contained"
          style={{
            borderRadius: '50px',
            width: '70%',
            height: 50,
            marginTop: 46,
            textTransform: 'none',
            background: '#5E52AD'
          }}>
          <Typography style={{ color: 'white' }}>Masuk disini</Typography>
        </Button>
      </Grid>
      <Grid item xs={12} className={classes.grid}>
        <Typography className={classes.syarat}>
          Dengan masuk dan mendaftar, Anda menyetujui{' '}
          <strong style={{ textDecoration: 'underline' }}>Syarat</strong>
        </Typography>
        <Typography className={classes.kebijakan}>
          <strong style={{ textDecoration: 'underline' }}>Penggunaan</strong>{' '}
          dan{' '}
          <strong style={{ textDecoration: 'underline' }}>
            Kebijakan Privasi
          </strong>
        </Typography>
      </Grid>
    </Container>
  );
}

export default withRouter(Login);
