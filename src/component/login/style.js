const styles = {
  diner: {
    width: '285px',
    height: '223px',
    paddingTop: '50px',
    paddingBottom: '20px',
    justiflyContent: 'center'
  },
  wellcome: {
    paddingTop: '10px',
    fontWeight: 'bold',
    fontSize: '24px',
    color: '#5E52AD',
    textAlign: 'center'
  },
  text: {
    textAlign: 'center',
    fontSize: '13px',
    paddingTop: '15px',
    color: '#8B8B8B',
    width: '303px'
  },
  syarat: {
    textAlign: 'center',
    fontSize: '11px',
    paddingTop: '50px',
    color: '#8B8B8B',
    width: '303px'
  },
  kebijakan: {
    textAlign: 'center',
    fontSize: '12px',
    color: '#8B8B8B',
    width: '303px',
    marginLeft: '10px',
    paddingBottom: '10px'
  },
  container: {
    border: '1px solid #e4e4e4',
    height: '100vh'
  },
  grid: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  gridAtas: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  }
};
export default styles;
