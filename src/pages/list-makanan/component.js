import React, { useState, useEffect } from 'react';
import Container from '@material-ui/core/Container';
import Appbar from '../../component/appbar';
import Promo from '../../component/promo';
import { getListMakanan } from '../../service/list-makanan';
import Card from '../../component/card-makanan';
function Listmakanan(props) {
  const {} = props;
  const [food, setFood] = useState([]);

  useEffect(() => {
    const getFood = async () => {
      const food = await getListMakanan();
      setFood(food);
      console.log();
    };
    getFood();
  }, []);
  return (
    <Container maxWidth="xs">
      <Appbar />
      <Promo />
      {food.map(item => {
        return <Card item={item} />;
      })}
    </Container>
  );
}
export default Listmakanan;
