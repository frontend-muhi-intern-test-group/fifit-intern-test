import React from 'react';
import Login from '../../component/login'
import Container from '@material-ui/core/Container'

function Home() {
  return (
     <Container maxWidth='xs'>
       <Login />
     </Container>
  );
}
export default Home;
