import axios from 'axios';

export const getListMakanan = async () => {
  const response = await axios.get('./local-data/list-makanan.json');
  return response.data.data;
};
