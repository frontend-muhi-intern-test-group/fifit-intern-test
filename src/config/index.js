import axios from 'axios';

export const axiosInstance = axios;

axiosInstance.interceptors.response.use(
  function(response) {
    console.log(response.data.data);
    return response.data.data;
  },
  function(error) {
    console.log(error);
    return Promise.reject(error);
  }
);
