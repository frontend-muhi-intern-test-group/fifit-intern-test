import React from 'react';
import Home from './pages/home';
import Listmakanan from './pages/list-makanan';
import { Route, Switch } from 'react-router-dom';

function App() {
  return (
    <React.Fragment>
     <Switch>
     <Route path="/" exact component={Home} />
     <Route path="/List-makanan" exact component={Listmakanan} />
     </Switch>
    </React.Fragment>
  );
}

export default App;
